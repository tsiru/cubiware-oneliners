module A2
  # umber from 1 to 20  create method calc(num) that produces an output as
  # https://gist.github.com/roxer/50166faa4da556b8128711535a3cf7c9
  def calc(num)
    (1..num).map { |i| (1..num).map { |j| '%-9x' % -(i*j) }.join.strip }.join("\n")
  end
end
