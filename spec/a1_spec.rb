require 'rspec'

require_relative '../a1'

RSpec.describe A1 do
  include A1
  it { expect(calc '').to eq('0' * 26) }
  it { expect(calc 'Foo Bar').to eq '11000100000000100100000000' }
end
