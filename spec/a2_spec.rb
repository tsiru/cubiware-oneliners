require 'rspec'

require_relative '../a2'

RSpec.describe A2 do
  include A2

  let(:example_5) { <<EXAMPLE_5.rstrip }
..f      ..fe     ..fd     ..fc     ..fb
..fe     ..fc     ..fa     ..f8     ..f6
..fd     ..fa     ..f7     ..f4     ..f1
..fc     ..f8     ..f4     ..f0     ..fec
..fb     ..f6     ..f1     ..fec    ..fe7
EXAMPLE_5

  let(:example_10) { <<EXAMPLE_10.rstrip }
..f      ..fe     ..fd     ..fc     ..fb     ..fa     ..f9     ..f8     ..f7     ..f6
..fe     ..fc     ..fa     ..f8     ..f6     ..f4     ..f2     ..f0     ..fee    ..fec
..fd     ..fa     ..f7     ..f4     ..f1     ..fee    ..feb    ..fe8    ..fe5    ..fe2
..fc     ..f8     ..f4     ..f0     ..fec    ..fe8    ..fe4    ..fe0    ..fdc    ..fd8
..fb     ..f6     ..f1     ..fec    ..fe7    ..fe2    ..fdd    ..fd8    ..fd3    ..fce
..fa     ..f4     ..fee    ..fe8    ..fe2    ..fdc    ..fd6    ..fd0    ..fca    ..fc4
..f9     ..f2     ..feb    ..fe4    ..fdd    ..fd6    ..fcf    ..fc8    ..fc1    ..fba
..f8     ..f0     ..fe8    ..fe0    ..fd8    ..fd0    ..fc8    ..fc0    ..fb8    ..fb0
..f7     ..fee    ..fe5    ..fdc    ..fd3    ..fca    ..fc1    ..fb8    ..faf    ..fa6
..f6     ..fec    ..fe2    ..fd8    ..fce    ..fc4    ..fba    ..fb0    ..fa6    ..f9c
EXAMPLE_10

  let(:example_15) { <<EXAMPLE_15.rstrip }
..f      ..fe     ..fd     ..fc     ..fb     ..fa     ..f9     ..f8     ..f7     ..f6     ..f5     ..f4     ..f3     ..f2     ..f1
..fe     ..fc     ..fa     ..f8     ..f6     ..f4     ..f2     ..f0     ..fee    ..fec    ..fea    ..fe8    ..fe6    ..fe4    ..fe2
..fd     ..fa     ..f7     ..f4     ..f1     ..fee    ..feb    ..fe8    ..fe5    ..fe2    ..fdf    ..fdc    ..fd9    ..fd6    ..fd3
..fc     ..f8     ..f4     ..f0     ..fec    ..fe8    ..fe4    ..fe0    ..fdc    ..fd8    ..fd4    ..fd0    ..fcc    ..fc8    ..fc4
..fb     ..f6     ..f1     ..fec    ..fe7    ..fe2    ..fdd    ..fd8    ..fd3    ..fce    ..fc9    ..fc4    ..fbf    ..fba    ..fb5
..fa     ..f4     ..fee    ..fe8    ..fe2    ..fdc    ..fd6    ..fd0    ..fca    ..fc4    ..fbe    ..fb8    ..fb2    ..fac    ..fa6
..f9     ..f2     ..feb    ..fe4    ..fdd    ..fd6    ..fcf    ..fc8    ..fc1    ..fba    ..fb3    ..fac    ..fa5    ..f9e    ..f97
..f8     ..f0     ..fe8    ..fe0    ..fd8    ..fd0    ..fc8    ..fc0    ..fb8    ..fb0    ..fa8    ..fa0    ..f98    ..f90    ..f88
..f7     ..fee    ..fe5    ..fdc    ..fd3    ..fca    ..fc1    ..fb8    ..faf    ..fa6    ..f9d    ..f94    ..f8b    ..f82    ..f79
..f6     ..fec    ..fe2    ..fd8    ..fce    ..fc4    ..fba    ..fb0    ..fa6    ..f9c    ..f92    ..f88    ..f7e    ..f74    ..f6a
..f5     ..fea    ..fdf    ..fd4    ..fc9    ..fbe    ..fb3    ..fa8    ..f9d    ..f92    ..f87    ..f7c    ..f71    ..f66    ..f5b
..f4     ..fe8    ..fdc    ..fd0    ..fc4    ..fb8    ..fac    ..fa0    ..f94    ..f88    ..f7c    ..f70    ..f64    ..f58    ..f4c
..f3     ..fe6    ..fd9    ..fcc    ..fbf    ..fb2    ..fa5    ..f98    ..f8b    ..f7e    ..f71    ..f64    ..f57    ..f4a    ..f3d
..f2     ..fe4    ..fd6    ..fc8    ..fba    ..fac    ..f9e    ..f90    ..f82    ..f74    ..f66    ..f58    ..f4a    ..f3c    ..f2e
..f1     ..fe2    ..fd3    ..fc4    ..fb5    ..fa6    ..f97    ..f88    ..f79    ..f6a    ..f5b    ..f4c    ..f3d    ..f2e    ..f1f
EXAMPLE_15

  it { expect(calc 5).to eq example_5 }
  it { expect(calc 10).to eq example_10 }
  it { expect(calc 15).to eq example_15 }
end
