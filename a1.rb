module A1
  # for any given string e.g "Foo Bar" create method calc(str) that produces
  # an output as “11000100000000100100000000” where 0 and 1 are matching
  # letters from alphabet (26 chars)
  def calc(str)
    ('a'..'z').map { |char| str.downcase.include?(char) ? 1 : 0 }.join
  end
end
